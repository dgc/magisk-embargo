#!/system/bin/sh
# vim: ts=4 et
#
# Magisk only performs overlay mounts from $MODDIR/system to /system,
# but we want them in other places too, so we have our own bind-mounter.

HOME=${0%/*}
exec >$HOME/embargo.log 2>&1

SYSPREFIXES='
    /system/product/app
    /system/app
    /data/app
    /data/user/0
'
for user in /data/user/??*; do
	user=${user##*/}
	USERPREFIXES="$USERREFIXES /data/user/${user} /data/user_de/${user} /data/misc/profiles/cur/${user}"
done

touch /sdcard/embargo.mft
chown sdcard_rw /sdcard/embargo.mft

(
    cd ${HOME-/nowhere} || exit
    cat embargo.mft /sdcard/embargo.mft |
    sed -e 's/[ \t]*#.*//' |
    grep -v '^ *$' |
    sort |
    uniq |
    while read package; do
        for prefix in $SYSPREFIXES; do
            dir="$prefix/$package" &&
            [ -d "$dir" ] &&
            echo mount -obind $HOME/embargo.d "$dir" &&
            mount -obind $HOME/embargo.d "$dir"

            dir=$(eval echo "$prefix/$package-??????????????????????==") &&
            [ -d "$dir" ] &&
            echo mount -obind $HOME/embargo.d "$dir" &&
            mount -obind $HOME/embargo.d "$dir"
        done
        for prefix in $USERPREFIXES; do
            dir="$prefix/$package" &&
            [ -d "$dir" ] &&
            echo rm -rf "$dir" &&
            rm -rf "$dir"
        done
    done
)
