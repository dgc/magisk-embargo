This folder/app has been embargoed by Magisk+embargo.

To manage the embargo manifest:

adb shell
android$ su
android# cd /data/adb/modules/embargo
android# vi embargo.mft
android# reboot

Some example manifests appear in the module folder. You can build your
own from these, or by hunting around for base folder/package names in
the like of:

	- /system/app
	- /system/product/app
	- /data/app
	- /data/user/*/app
