# Embargo

Embargo helps you repress apps that you don't want, whether they're
userland apps or firmware apps, without removing them from your device.
It does this by giving you a "manifest file" listing package and folder
names as they appear in various `.../app` locations. Each time the
device boots, it will hunt down matching installed apps and "disappear"
them by bind-mounting over them.

If an embargoed app is removed and reinstalled, it will be disappeared
again upon reboot.


## Managing the embargo

To manage the embargo manifest:

	C:\> adb shell		# lol I don't really use windows
	android$ cd /sdcard
	android$ vi embargo.mft
	android$ su reboot

_or_

	C:\> adb shell		# lol I don't really use windows
	android$ su
	android# cd /data/adb/modules/embargo
	android# vi embargo.mft
	android# reboot

(You may of course use another text editor to edit the manifest, I just
don't know any other text editors to use as examples.)

Comments and blank lines are allowed in the manifest. In addition to the
`embargo.mft` file in the module folder, you may also list embargoed
folders in `/sdcard/embargo.mft`.


## Examples

Some example manifests appear in the module folder. You can build your
own from these, or by hunting around for base folder/package names in
the likes of:

	- /system/app
	- /system/product/app
	- /data/app
	- /data/user/*/app

Note that whereas system (firmware) apps usually get their own folders
with application names, userspace apps are commonly named with their
application bundle ID + a hyphen + a nonce. DO NOT INCLUDE THE HYPHEN
AND NONCE when listing packages in the manifest file. Embargo will
find the packages without them, and using them means that you lose the
embargo if the app is removed and reinstalled.

# Why?

There are a few motivators.

* It's an easy way to ensure that firmware bloat is cleanly repressed
  all the time, without having to skulk around trying to actually modify
  the system image.

* It's a way to ensure that unwanted apps remain unavailable for privacy,
  security, or protection reasons. For instance, I have a phone that my
  children can use to call their adults, but they cannot use Instagram,
  install games, browse the web, etc.


# Building the Magisk module zipfile

Just use `make`. You need `python3` available.

    git clone https://gitlab.com/dgc/magisk-embargo
	cd magisk-embargo
	make
	adb push embargo.zip /sdcard/Download


# History

## 0.1 - 20211204

