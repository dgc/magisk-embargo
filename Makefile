
MODULE=$(shell sh -c 'x=$$(basename $$(pwd)); echo $${x\#magisk-}')

$(MODULE).zip:
	python3 makezip $(MODULE).zip module
